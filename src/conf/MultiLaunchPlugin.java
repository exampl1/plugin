package conf;

import java.util.*;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.*;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.google.common.base.*;
import com.google.common.collect.*;

public class MultiLaunchPlugin extends AbstractUIPlugin {

	public static final String PLUGIN_ID = "conf.ui.MultiLaunchPlugin";
	
	public static final String NESTED_CONFIGURATIONS = "nestedConfigurations";
	
	public static final String COMPOSITE_CONF_TYPE_ID = "conf.ui.MultiLaunchPlugin";
	
	private static MultiLaunchPlugin plugin;
	
	public MultiLaunchPlugin() {
	}

	/*
	 * org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}
	
	/*
	 * org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static void setConfigurations(ILaunchConfigurationWorkingCopy configuration,
			Iterable <?> configurationName) {
		String configs = Joiner.on("@").skipNulls().join(configurationName);
		configuration.setAttribute(NESTED_CONFIGURATIONS, configs);
	}

	/*
	 * @param configuration
	 *            composite launch configuration
	 * @param mode
	 *            "run" or "debug" or null
	 * @return list of child configurations contained in the given multiple configuration
	 * @throws CoreException
	 */
	public static List <ILaunchConfiguration> getChildConfigurations(ILaunchConfiguration configuration,
			String mode) throws CoreException {
		
		List <ILaunchConfiguration> childConfigs = Lists.newLinkedList();
		
		Iterable <String> configs = Splitter.on("@").split(configuration.getAttribute(NESTED_CONFIGURATIONS, ""));
		Set <String> childConfNames = Sets.newHashSet(configs);
		
		ILaunchConfiguration[] allConfigurations = DebugPlugin.getDefault().getLaunchManager()
				.getLaunchConfigurations();
		
		for (ILaunchConfiguration childConfig : allConfigurations) {
			try {
				if (childConfNames.contains(childConfig.getName())
						&& (mode == null || childConfig.getType().supportsMode(mode))) {
					childConfigs.add(childConfig);
				}
			} catch (CoreException ex) {
				log(ex);
			}
		}
		return childConfigs;
	}

	/*
	 * @param configuration
	 * @return
	 * @throws CoreException
	 * @see CompositeLaunchPlugin#getChildConfigurations(ILaunchConfiguration, String)
	 */
	public static List <ILaunchConfiguration> getChildConfigurations(ILaunchConfiguration configuration)
			throws CoreException {
		return getChildConfigurations(configuration, null);
	}

	/*
	 * @return the shared instance
	 */
	public static MultiLaunchPlugin getDefault() {
		return plugin;
	}

	public static void log(CoreException ex) {
		MultiLaunchPlugin.getDefault().getLog().log(ex.getStatus());
	}
}
