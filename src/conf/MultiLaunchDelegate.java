package conf;

import org.eclipse.core.runtime.*;
import org.eclipse.debug.core.*;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;

public class MultiLaunchDelegate implements ILaunchConfigurationDelegate {

	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		for (ILaunchConfiguration childConf : MultiLaunchPlugin.getChildConfigurations(configuration, mode)) {
			childConf.launch(mode, monitor);
		}
	}
}
