package conf.ui;

import org.eclipse.debug.ui.*;

public class LaunchConfigurationTabGroup extends AbstractLaunchConfigurationTabGroup {

	public LaunchConfigurationTabGroup() {		
	}

	@Override
	public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
		setTabs(new ILaunchConfigurationTab[] { new MultiLaunchConfigurationTab(), new CommonTab() });
	}

}
