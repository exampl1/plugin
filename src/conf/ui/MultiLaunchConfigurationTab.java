package conf.ui;

import java.util.Arrays;
import java.util.List;

import conf.MultiLaunchPlugin;

import com.google.common.base.Predicates;
import com.google.common.collect.*;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.*;
import org.eclipse.debug.internal.ui.launchConfigurations.*;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;
import org.eclipse.ui.model.WorkbenchViewerComparator;

public class MultiLaunchConfigurationTab extends AbstractLaunchConfigurationTab {
	
	private CheckboxTreeViewer viewer;
	
	private ITreeContentProvider contentProvider;

	/*
	 * Selected children launch configurations with some child
	 * launch configurations with unsupported modes
	 */
	
	private List <String> unsupportedChildConfigs;
	
	@Override
	public void createControl(Composite parent) {
		viewer = new ContainerCheckedTreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		setControl(viewer.getTree());
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
	}

	protected void refreshLaunchConfigurationDialog() {
		getLaunchConfigurationDialog().updateButtons();
		getLaunchConfigurationDialog().updateMessage();
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		
		final String mode = getLaunchConfigurationDialog().getMode();
		
		viewer.setLabelProvider(new DecoratingLabelProvider(DebugUITools.newDebugModelPresentation(), PlatformUI
				.getWorkbench().getDecoratorManager().getLabelDecorator()));
		viewer.setComparator(new WorkbenchViewerComparator());
		contentProvider = new LaunchConfigurationTreeContentProvider(mode, null);
		viewer.setContentProvider(contentProvider);
		viewer.addFilter(new LaunchGroupFilter(DebugUITools.getLaunchGroup(configuration, mode)));
		viewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
		
		List <Object> checkedElements = Lists.newArrayList();
		
		unsupportedChildConfigs = Lists.newLinkedList();
		
		try {
			for (ILaunchConfiguration childConf : MultiLaunchPlugin.getChildConfigurations(configuration)) {
				try {
					if (childConf.getType().supportsMode(mode)) {
						checkedElements.add(childConf);
					} else {
						unsupportedChildConfigs.add(childConf.getName());
					}
				} catch (CoreException e) {
					MultiLaunchPlugin.log(e);
				}
			}
			
			checkedElements.addAll(MultiLaunchPlugin.getChildConfigurations(configuration, mode));
			viewer.setCheckedElements(checkedElements.toArray());
		} catch (CoreException e) {
			MultiLaunchPlugin.log(e);
		}
		
		viewer.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				refreshLaunchConfigurationDialog();
			}
		});
	}
	
	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		
		List <Object> checkedElements = Arrays.asList(viewer.getCheckedElements());
		MultiLaunchPlugin.setConfigurations(configuration, Iterables.concat(
				Iterables.filter(checkedElements, Predicates.instanceOf(ILaunchConfiguration.class)),
				unsupportedChildConfigs));
	}
	
	@Override
	public boolean isValid(ILaunchConfiguration configuration) {
		
		String name = configuration.getName();
		
		for (Object element : viewer.getCheckedElements()) {
			if (element instanceof ILaunchConfiguration) {
				if (conf_contains((ILaunchConfiguration) element, name)) {
					setErrorMessage("Unable to select dependent configurations");
					return false;
				}
			} else if (!contentProvider.hasChildren(element)) {
					setErrorMessage("Unable to select empty configuration type");
					return false;
			}
		}

		setErrorMessage(null);
		return true;
	}

	/*
	 * @param configuration
	 * @param name
	 * @return true if the given configuration has composite type
	 * 	with child configuration with the given name 
	 */
	private boolean conf_contains(ILaunchConfiguration configuration, String name) {
		if (name.equals(configuration.getName())) {
			return true;
		}
		
		try {
			final String mode = getLaunchConfigurationDialog().getMode();
			if (MultiLaunchPlugin.COMPOSITE_CONF_TYPE_ID.equals(configuration.getType().getIdentifier())) {
				for (ILaunchConfiguration childConfig : MultiLaunchPlugin.getChildConfigurations(configuration, mode)) {
					if (conf_contains(childConfig, name)) {
						return true;
					}
				}
			}
		} catch (CoreException e) {
			MultiLaunchPlugin.log(e);
		}
		
		return false;
	}
	
	@Override
	public String getName() {
		return "Launches";
	}

	@Override
	public boolean canSave() {
		return true;
	}
}
